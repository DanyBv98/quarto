//
// Created by dany on 12.10.2018.
//
#include "Piece.h"

std::ostream &operator<<(std::ostream &outputStream, const Piece &piece)
{
    return outputStream << static_cast<int>(piece.m_body) << " " << static_cast<int>(piece.m_color) << " " <<
        static_cast<int>(piece.m_height) << " " << static_cast<int>(piece.m_shape);
}

Piece::Piece(Piece::Body body, Piece::Color color, Piece::Height height, Piece::Shape shape) :
    m_body(body),
    m_color(color),
    m_height(height),
    m_shape(shape)
{
    static_assert(sizeof(*this) == 1, "Size of Piece is not 1");
}

Piece::Body Piece::GetBody() const {
    return m_body;
}

Piece::Color Piece::GetColor() const {
    return m_color;
}

Piece::Height Piece::GetHeight() const {
    return m_height;
}

Piece::Shape Piece::GetShape() const {
    return m_shape;
}
