#include <iostream>
#include "Piece.h"

int main() {
    Piece piece(Piece::Body::Full, Piece::Color::Light, Piece::Height::Tall, Piece::Shape::Square);
    std::cout << piece;

    return 0;
}